<img align="right" src="img/logo.png" width="220" />

# flow3-rs
### Rust board-support / runtime for the flow3r badge

This repo provides a pure-rust board support crate / runtime + drivers for the hardware on the badge

> ## This is very much alpha, do not rely on completeness or stability 
> This project is still WIP, many things do not work yet, and many are subject to change.
> See the list below for what works and what doesn't.
> If you want stability and feature-completeness, you should take a look at https://docs.flow3r.garden for info on the excellent C/MicroPython firmware.
> So with that out of the way, continue reading if you want to play with Rust on your badge!

## State of implementation

- [x] Display: working with embedded-graphics and DMA, blitting still needs to go on second core for performance
- [x] LEDs
- [x] Port expanders
- [x] Input rockers
- [ ] IMU: most probably working, could not fully test due to broken IMU on my prototype -> HELP APPRECIATED FOR TESTING!
- [x] Captouch: driver ~~partially~~ mostly implemented, ~~however currently broken~~, working again, API needs some love though
- [x] BadgeLink/BadgeNet: should be mostly working now, interoperability with MicroPython FW not tested -> HELP APPRECIATED FOR TESTING!
- [ ] SD-Card: not implemented yet
- [ ] Barometer: should come mostly for free with IMU, but could not test that yet
- [ ] Audio: not implemented yet

If you are interested in implementing or testing one of the missing features, I am always happy for any help. Just open an issue or reach out via matrix (@zdmx:xatellite.space) or e-mail.

## Development setup

The hardware of the badge is based on the ESP32S3 which uses the Xtensa-Architecture. Xtensa is currently not supported in mainline LLVM/rustc, so we have to install the Espressif-forks of the toolchain.

The easiest way for that is to use the `espup` tool provided by Espressif.
https://github.com/esp-rs/espup

You can install espup with cargo, and then install the toolchain with espup:
```
cargo install espup
espup install
```

Make sure your cargo bin directory (usually `$HOME/.cargo/bin`) is in PATH.

`espup` will place a file called `export-esp.sh` in your Home-Directory which you need to source to activate the toolchain.

Additionally, you should install `cargo-espflash` for easy flashing

```
cargo install cargo-espflash
```

### Flashing

To flash from the official firmware to a Rust binary, you have to put your badge into bootloader mode by holding the left shoulder button during poweron. After a few seconds, a `Espressif USB JTAG/serial debug unit` should appear on the USB bus.
This is only nessecary when flashing from the official firmware to Rust, because the C firmware takes over the usb port.
When reflashing a Rust binary, you should not have to enter bootloader mode. 

Now you can flash your code onto the badge using `espflash` by running

```
cargo espflash flash --monitor --release
```
which also immediately attaches to stdout of the badge.
If it does not attach after flashing from the official firmware to a Rust binary, restart your badge and try the flashing again.

The `--release` flag is optional, however some things (especially badgenet) might not work properly without it due to timing problems without optimizations.

### Debugging
The ESP32-S3 contains a JTAG controller on board. According to [esp-rs](https://esp-rs.github.io/book/tooling/debugging/openocd.html), this can be utilized by using openocd. The normal version of openocd doesn´t support Xtensa, so we need [Espressif's fork](https://github.com/espressif/openocd-esp32). Follow the build instructions on that page to build and install this version of openocd. 

#### Visual Studio Code 
To debug in Visual Studio Code, the [esp-rs book](https://esp-rs.github.io/book/tooling/debugging/vscode.html) advices to use the extension [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug). The page says there only is onboard JTAG for C3, but according to [this page](https://docs.espressif.com/projects/esp-idf/en/latest/esp32s3/api-guides/jtag-debugging/index.html#jtag-debugging-selecting-jtag-adapter) the ESP32S3 (the one in flow3r) has this as well. Doing so, we combine the example `launch.json`:

```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            // more info at: https://github.com/Marus/cortex-debug/blob/master/package.json
            "name": "Attach",
            "type": "cortex-debug",
            "request": "attach", // attach instead of launch, because otherwise flash write is attempted, but fails
            "cwd": "${workspaceRoot}",
            "executable": "${workspaceRoot}/target/xtensa-esp32s3-none-elf/debug/flow3-rs",
            "servertype": "openocd",
            "interface": "jtag",
            "gdbPath": "/home/user/.espressif/tools/xtensa-esp-elf-gdb/12.1_20221002/xtensa-esp-elf-gdb/bin/xtensa-esp32s3-elf-gdb",
            // "armToolchainPath": "/home/user/.rustup/toolchains/esp/xtensa-esp32s3-elf/esp-12.2.0_20230208/xtensa-esp32s3-elf/bin",
            "svdFile": "../../esp-pacs/esp32/svd/esp32.svd",
            "toolchainPrefix": "xtensa-esp32s3-elf",
            "openOCDPreConfigLaunchCommands": [
                "set ESP_RTOS none"
            ],
            "serverpath": "/usr/local/bin/openocd",
            "configFiles": [
                "board/esp32s3-builtin.cfg"
            ],
            "overrideAttachCommands": [
                "set remote hardware-watchpoint-limit 2",
                "mon halt",
                "flushregs"
            ],
            "overrideRestartCommands": [
                "mon reset halt",
                "flushregs",
                "c"
            ],
            "showDevDebugOutput": "raw"
        }
    ]
}
```
The page suggests to replace the values for `executable`, `svdFile`, `serverpath` paths, and `toolchainPrefix`. If you install openocd, call `whereis openocd` to find the location of Espressif's fork of openocd we just installed. Fill this path for `serverpath`. To get the `svdfile`, clone [this repo](https://github.com/esp-rs/esp-pacs.git) parallel to this projects folder, so that you can use the default value for `svdFile`. Set the toolchain to the one for the esp32s3, `xtensa-esp32s3-elf`. Set `executable` to `"${workspaceRoot}/target/xtensa-esp32s3-none-elf/debug/flow3-rs"`

In VSCode Run and Debug, run the Attach configuration. This starts openocd as well.

#### Manual openocd
Build the projecte for the flow3r `cargo build --target xtensa-esp32s3-none-elf`. After building, start openocd `openocd -f board/esp32s3-builtin.cfg` in one terminal. In another terminal, start espressif's gdb version, connecting to the `flow3-rs` ELF file (does not have the ELF extension)  `~/.espressif/tools/xtensa-esp-elf-gdb/12.1_20221002/xtensa-esp-elf-gdb/bin/xtensa-esp32s3-elf-gdb target/xtensa-esp32s3-none-elf/debug/flow3-rs`. I believe this is installed using `espup` before.

## Further documentation / Apidocs

TBD
