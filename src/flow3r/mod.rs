use hal::{Uart, Rng};
use hal::peripherals::{UART0, UART1};

use self::badgelink::BadgeLink;
use self::badgelink::badgenet::{BadgenetUartLeft, BadgenetUartRight};
use self::captouch::CaptouchHandler;
use self::display::Display;
use self::imu::ImuHandler;
use self::input::InputHandler;
use self::leds::Leds;

pub mod badgelink;
pub mod captouch;
pub mod display;
pub mod imu;
pub mod input;
pub mod leds;
pub mod sdcard;
pub mod ui;

pub struct Flow3r {
    pub badgelink: BadgeLink,
    pub captouch: CaptouchHandler,
    pub display: Display,
    pub imu: ImuHandler,
    pub inputs: InputHandler,
    pub leds: Leds,
    pub uart0: BadgenetUartLeft,
    pub uart1: BadgenetUartRight,
    pub rng: &'static mut Rng,
}

impl Flow3r {
    pub fn new(
        badgelink: BadgeLink,
        captouch: CaptouchHandler,
        display: Display,
        imu: ImuHandler,
        inputs: InputHandler,
        leds: Leds,
        uart0: BadgenetUartLeft,
        uart1: BadgenetUartRight,
        rng: &'static mut Rng,
    ) -> Self {
        Self {
            badgelink,
            captouch,
            display,
            imu,
            inputs,
            leds,
            uart0,
            uart1,
            rng
        }
    }
}
